const Discord = require('discord.js');
const system = require('child_process');

const token = require('./token.json');
const config = require('./config.json'); 

const client = new Discord.Client();



var prefix = config.prefix;
var files = config.files;
var keys = Object.keys(files);
client.on('message',function onMessage(message){
    if(message.author.bot)
        return;
    
    var content = message.content;
    if(content.length<prefix.length||content.substring(0,prefix.length)!=prefix)
        return;

    var arg = content.substring(prefix.length);
    arg = arg.replace(/\s+/,"");
    var time = new Date();
    var timeString = `${time.getMonth()}:${time.getDate()}:${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`;
    var didntRun =  keys.every( (value)=>{
        
        if(value==arg){
            var filename = files[value];
            console.log(`[${timeString}]User ${message.author.username} in server ${message.guild} ran file: ${filename}`);

            runFile(filename);

            message.channel.send('Ran the local command: '+value)
            return false;
        }
        return true;

    })
    if(didntRun){
        message.reply('Not a valid command!');
        console.log(`[${timeString}]User ${message.author.username} in server ${message.guild} unsuccesfully ran the sub-argument ${arg}`);
    }
    
});

function runFile(filename){
    var filesplit = filename.split(/[\\/]/);
    var file = filesplit.pop();
    var directory = filesplit.join('/');

    system.exec(file,{cwd:directory});
}

client.login(token.token);