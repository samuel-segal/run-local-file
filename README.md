# Run Local File

A Discord bot that allows users to run local files by use of commands. Run this on the device with the files you want to access, and configure it accordingly

Put the Discord token in the token.json file

To create an argument-file relationship, introduce the argument as a key in the files portion of the config.json file, and set the value of the key to be the file you want to execute